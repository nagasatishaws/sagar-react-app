FROM node:16.13.0

RUN mkdir /app

WORKDIR /app

COPY package.json /app

COPY package-lock.json /app

COPY  auth  /app

COPY  finops  /app

COPY lib-app  /app

COPY store  /app

COPY . /app

RUN npm install
RUN npx lerna bootstrap

CMD [ "npm" , "start" ]
